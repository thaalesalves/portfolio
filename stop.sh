#!/bin/bash

if output=$(ps -ef | grep "portfolio"); then
    set -- $output
    pid=$2
    kill $pid
    sleep 2
    kill -9 $pid > /dev/null 2>&1
        echo "Processo do Portfolio morto"
fi
