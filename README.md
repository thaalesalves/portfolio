Portfolio
====
# Projeto
Este é o projeto de desenvolvimento do meu portfolio online, usando como base o template [Orbit](https://github.com/xriley/Orbit-Theme) de [Xiaoying Riley](https://github.com/xriley/), portado para React por [S.Berkay Aydın](https://github.com/sbayd/react-cv-template). O template será portado de um site estático para um site com backend.

# Tecnologias
* Node.js
* React
* jQuery
* FontAwesome
* Bootstrap