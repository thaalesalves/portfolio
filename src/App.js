import React, { Component } from 'react';
import PropTypes from 'prop-types';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.css';
import './assets/theme/dark-style.css';
import Section from './components/shared/section';
import Sidebar from './components/sidebar';
import Experiences from './components/experiences';
import Education from './components/education';
import Projects from './components/projects';
import Tags from './components/tags';
import Courses from './components/courses';
import Certifications from './components/certifications';

export default class CV extends Component {
  renderCoursesSection() {
    if (this.props.courses) {
      return (<Courses {...this.props.courses} />);
    }
    return null;
  }

  renderCertificationsSection() {
    if (this.props.certifications) {
      return (<Certifications {...this.props.certifications} />);
    }
    return null;
  }

  renderEducationSection() {
    if (this.props.education) {
      return (<Education {...this.props.education} />);
    }
    return null;
  }

  renderExperiencesSection() {
    if (this.props.experiences) {
      return (<Experiences {...this.props.experiences} />);
    }
    return null;
  }

  renderProjectsSection() {
    if (this.props.projects) {
      return (<Projects {...this.props.projects} />);
    }
    return null;
  }

  renderTags() {
    if (this.props.tags) {
      return (<Tags {...this.props.tags} />);
    }
    return null;
  }

  renderCareerProfile() {
    const { icon, sectionTitle, description } = this.props.careerProfile;
    const innerContent = (<div className="summary" dangerouslySetInnerHTML={{ __html: description }} />);
    return (
      <Section
        className="summary-section"
        icon={icon || 'user'}
        title={sectionTitle || 'Career Profile'}
      >
        {innerContent}
      </Section>
    );
  }

  render() {
    return (
      <div className="wrapper">
        <Sidebar
          {...this.props.profile}
        />
        <div className="main-wrapper">
          {this.renderCareerProfile()}
          {this.renderExperiencesSection()}
          {this.renderProjectsSection()}
          {this.renderEducationSection()}
          {this.renderCertificationsSection()}
          {this.renderCoursesSection()}
          {this.renderTags()}
        </div>
      </div>
    );
  }
}

CV.propTypes = {
  profile: PropTypes.shape().isRequired,
  careerProfile: PropTypes.shape().isRequired,
  experiences: PropTypes.shape().isRequired,
  education: PropTypes.shape().isRequired,
  certifications: PropTypes.shape().isRequired,
  courses: PropTypes.shape().isRequired,
  projects: PropTypes.shape().isRequired,
  tags: PropTypes.shape().isRequired
};
