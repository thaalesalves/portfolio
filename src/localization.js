import i18next from './i18nextConf'

const translate = (key) => i18next.t(key);

export const local = {
    translate
}

export default local;