import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Section from './shared/section';

export default class Education extends Component {
  renderListItem(item, i) {
    return (
      <div className="item" key={`exp_item_${i}`}>
        <div className="meta">
          <div className="upper-row">
            <h3 className="job-title">{item.course}</h3>
            <div className="time">{item.date}</div>
          </div>
          {this.renderCompanySection(item.institution, item.institutionLink)}
        </div>
        <div className="details">
          <p dangerouslySetInnerHTML={{ __html: item.description }} />
        </div>
      </div>
    );
  }
  renderCompanySection(institution, institutionLink) {
    if (institution) {
      return (<div className="company"> <a href={institutionLink} target="_blank">{institution}</a></div>);
    }
    return null;
  }
  render() {
    const { icon, sectionTitle, list } = this.props;
    return (
      <Section
        className="experiences-section"
        icon={icon || 'graduation-cap'}
        title={sectionTitle || 'Education'}
        id="experiences"
      >
        {list.map((item, i) => {
          return this.renderListItem(item, i);
        })}
      </Section>
    );
  }
}

Education.propTypes = {
  list: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  sectionTitle: PropTypes.string.isRequired,
  icon: PropTypes.string
};

