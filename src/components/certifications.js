import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Section from './shared/section';

export default class Certifications extends Component {
  renderListItem(item, i) {
    return (
      <div className="item" key={`exp_item_${i}`}>
        <div className="meta">
          <div className="upper-row">
            <h3 className="job-title"><a style={{ color: '#3F4650' }} href={item.certificateLink} target="_blank">{item.title}</a></h3>
            <div className="time">{item.date}</div>
          </div>
          {this.renderCompanySection(item.company, item.certificateLink, item.certificateText)}
        </div>
      </div>
    );
  }

  renderCompanySection(company, certificateLink, certificateText) {
    if (company) {
      if (certificateText) {
        return (<div className="company"> {company} • <a href={certificateLink} target="_blank">{certificateText}</a></div>);
      }
      return (<div className="company"> {company}</div>);
    }
    return null;
  }

  render() {
    const { icon, sectionTitle, list } = this.props;
    return (
      <Section
        className="experiences-section"
        icon={icon || 'graduation-cap'}
        title={sectionTitle || 'Education'}
        id="experiences"
      >
        {list.map((item, i) => {
          return this.renderListItem(item, i);
        })}
      </Section>
    );
  }
}

Certifications.propTypes = {
  list: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  sectionTitle: PropTypes.string.isRequired,
  icon: PropTypes.string
};

