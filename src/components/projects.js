import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Section from './shared/section';

export default class Projects extends Component {
  renderListItem(item, i) {
    return (
      <div className="item" key={`exp_item_${i}`}>
        <div className="meta">
          <div className="upper-row">
            <h3 className="job-title"><a style={{ color: '#3F4650' }} href={item.projectLink} target="_blank">{item.projectName}</a></h3>
            <div className="time">{item.date}</div>
          </div>
          {this.renderCompanyLink(item.company, item.companyLink, item.myTitle)}
        </div>
        <div className="details">
          <p dangerouslySetInnerHTML={{ __html: item.description }} />
        </div>
      </div>
    );
  }

  renderCompanyLink(company, companyLink, myTitle) {
    if (company) {
      return (<div className="company"> <a href={companyLink} target="_blank">{company}</a> {myTitle || ''}</div>);
      // return (<div className="company" style={{ color: '#2d7788' }}> <a href={companyLink} target="_blank">{company}</a> {myTitle || ''}</div>);
    }
    return null;
  }

  render() {
    const { icon, sectionTitle, list } = this.props;
    return (
      <Section
        className="experieces-section"
        icon={icon || 'archive'}
        title={sectionTitle || 'Experiences'}
        id="experiences"
      >
        {list.map((item, i) => {
          return this.renderListItem(item, i);
        })}
      </Section>
    );
  }
}

Projects.propTypes = {
  list: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  sectionTitle: PropTypes.string.isRequired,
  icon: PropTypes.string
};

