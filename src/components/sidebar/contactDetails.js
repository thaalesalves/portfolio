import React, { Component } from 'react';
import PropTypes from 'prop-types';
import i18next from '../../i18nextConf'

export default class ContactDetails extends Component {
  renderListItem(className, data, iconName, link) {
    if (!data) { return null; }

    return (
      <li className={className}>
        <i className={`fa ${iconName}`} />
        <a href={`${link}`} target="_blank"> {data} </a>
      </li>
    );
  }
  render() {
    return (
      <div className="contact-container container-block">
        <ul className="list-unstyled contact-list">
          {this.renderListItem('email', this.props.mail, 'fa-envelope', 'mailto:thales@thalesalv.es')}
          {this.renderListItem('website', this.props.website, 'fa-globe', 'https://thalesalv.es')}
          {this.renderListItem('linkedin', this.props.linkedin, 'fa-linkedin', 'https://linkedin.com/in/thaalesalves')}
          {this.renderListItem('github', this.props.github, 'fa-github', 'https://github.com/thaalesalves')}
          {this.renderListItem('gitlab', this.props.gitlab, 'fa-gitlab', 'https://gitlab.com/thaalesalves')}
          {this.renderListItem('twitter', this.props.twitter, 'fa-twitter', 'https://twitter.com/thaalesalves')}
          {this.renderListItem('instagram', this.props.instagram, 'fa-instagram', 'https://instagram.com/guaruaru35mm')}
          {this.renderListItem('facebook', this.props.facebook, 'fa-facebook', 'https://facebook.com/thaalesalves')}
          {this.renderListItem('wordpress', this.props.wordpress, 'fa-wordpress', 'https://blog.thalesalv.es/')}
          {this.renderListItem('curriculum', this.props.curriculum, 'fa-certificate', i18next.t('careerProfile.curriculum-link'))}
        </ul>
      </div >
    );
  }
}

ContactDetails.propTypes = {
  mail: PropTypes.string.isRequired,
  website: PropTypes.string,
  linkedin: PropTypes.string,
  github: PropTypes.string,
  gitlab: PropTypes.string,
  twitter: PropTypes.string,
  facebook: PropTypes.string,
  instagram: PropTypes.string,
  wordpress: PropTypes.string,
  curriculum: PropTypes.string,
};

ContactDetails.defaultProps = {
  website: null,
  linkedin: null,
  github: null,
  gitlab: null,
  twitter: null,
  facebook: null,
  instagram: null,
  wordpress: null,
  curriculum: null,
};
