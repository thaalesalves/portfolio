import local from './localization'
import dateAndTime from 'date-and-time'

const getAge = () => {
    let ageDifMs = Date.now() - new Date(1995, 4, 6).getTime();
    let ageDate = new Date(ageDifMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

const getTimeSince = (dateFrom, dateTo) => {

    const formattedTo = dateAndTime.format(dateTo, 'MM.YYYY') == dateAndTime.format(new Date(), 'MM.YYYY') ? local.translate('words.current') : dateAndTime.format(dateTo, 'MM.YYYY');
    const formattedFrom = dateAndTime.format(dateFrom, 'MM.YYYY');
    const diffInDays = Math.ceil((new Date(dateTo).getTime() - new Date(dateFrom).getTime()) / (1000 * 60 * 60 * 24))
    return ` ${formattedFrom} - ${formattedTo} (${getFormatedStringFromDays(diffInDays)})`
}

const getFormatedStringFromDays = (numberOfDays) => {
    let years = Math.floor(numberOfDays / 365);
    let months = Math.floor(numberOfDays % 365 / 30);
    let yearsDisplay = years > 0 ? years + (years == 1 ? ` ${local.translate('words.year')} ` : ` ${local.translate('words.years')} `) : "";
    let monthsDisplay = months > 0 ? months + (months == 1 ? ` ${local.translate('words.month')} ` : ` ${local.translate('words.months')} `) : "";
    return (yearsDisplay + monthsDisplay).trim();
}

export const time = {
    getAge,
    getTimeSince,
    getFormatedStringFromDays
}

export default time;
