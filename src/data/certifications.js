import i18next from '../i18nextConf'

export const certifications = {
    sectionTitle: `${i18next.t('certifications.title')}`,
    description: null,
    icon: null,
    list: [
      {
        title: 'IBM Mentor',
        company: 'IBM',
        date: '01.2021',
        certificateLink: 'https://www.youracclaim.com/badges/7c95961d-73ac-49bf-a338-1b3c4147035d?source=linked_in_profile',
        certificateText: `${i18next.t('certifications.certificateText')}`
      },
      {
        title: 'Certified Specialist in OpenShift Application Development',
        company: 'Red Hat',
        date: '01.2021 - 01.2024',
        certificateLink: 'https://rhtapps.redhat.com/certifications/badge/verify/6YXJWV5QF4CBIAVCL54LK4SQAMAEQU3CUPSQX2KSDXT6RW46LQ3YWP2PKMJOB2FESKFUN2GLGGL67UAA4DNI6PQU27PFU2ZMLS26POA=',
        certificateText: `${i18next.t('certifications.certificateText')}`
      },
      {
        title: 'Certified Specialist in Containers and Kubernetes',
        company: 'Red Hat',
        date: '12.2020 - 12.2023',
        certificateLink: 'https://rhtapps.redhat.com/certifications/badge/verify/6YXJWV5QF4CBIAVCL54LK4SQAMAEQU3CUPSQX2KSDXT6RW46LQ33TZNCC5VGOAYPFY7HVVIGB5XKUTI5W6QLZX6UMV3D6ILAY7YA4GY=',
        certificateText: `${i18next.t('certifications.certificateText')}`
      }
    ]
  };
  
  export default certifications;
  