import profile from './profile';
import careerProfile from './careerProfile';
import experiences from './experiences';
import projects from './projects';
import tags from './tags';
import education from './education';
import certifications from './certifications';
import courses from './courses';

export const data = {
  profile,
  careerProfile,
  experiences,
  projects,
  certifications,
  education,
  courses,
  tags
};

export default data;
