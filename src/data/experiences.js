import i18next from '../i18nextConf'
import time from '../timeFunctions'
import dateAndTime from 'date-and-time'

export const experiences = {
  sectionTitle: `${i18next.t('experience.title')}`,
  icon: null,
  description: null,
  list: [
    {
      title: `${i18next.t('experience.experiences.primeit.title')}`,
      company: 'PrimeIT',
      description: `${i18next.t('experience.experiences.primeit.description')}`,
      companyLink: 'https://primeit.pt/',
      companyShortDetail: `${i18next.t('general.locations.lisbon')}`,
      date: time.getTimeSince(
        dateAndTime.parse('08-2021', 'MM-YYYY'),
        new Date()
      ),
    },
    {
      title: `${i18next.t('experience.experiences.ipc.title')}`,
      company: `${i18next.t('experience.experiences.ipc.company')}`,
      description: `${i18next.t('experience.experiences.ipc.description')}`,
      companyLink: 'https://www.ipc.be/',
      companyShortDetail: `${i18next.t('general.locations.lisbon')}`,
      date: time.getTimeSince(
        dateAndTime.parse('07-2023', 'MM-YYYY'),
        new Date()
      ),
    },
    {
      title: `${i18next.t('experience.experiences.vizrt.title')}`,
      company: `${i18next.t('experience.experiences.vizrt.company')}`,
      description: `${i18next.t('experience.experiences.vizrt.description')}`,
      companyLink: 'https://www.vizrt.com/',
      companyShortDetail: `${i18next.t('general.locations.lisbon')}`,
      date: time.getTimeSince(
        dateAndTime.parse('06-2022', 'MM-YYYY'),
        dateAndTime.parse('07-2023', 'MM-YYYY')
      ),
    },
    {
      title: `${i18next.t('experience.experiences.aris.title')}`,
      company: `${i18next.t('experience.experiences.aris.company')}`,
      description: `${i18next.t('experience.experiences.aris.description')}`,
      companyLink: 'https://www.arisglobal.com/',
      companyShortDetail: `${i18next.t('general.locations.lisbon')}`,
      date: time.getTimeSince(
        dateAndTime.parse('08-2021', 'MM-YYYY'),
        dateAndTime.parse('05-2022', 'MM-YYYY')
      ),
    },
    {
      title: `${i18next.t('experience.experiences.ibm.title')}`,
      company: 'IBM',
      description: `${i18next.t('experience.experiences.ibm.description')}`,
      companyLink: 'http://ibm.com/',
      companyShortDetail: `${i18next.t('general.locations.saopaulo')}`,
      date: time.getTimeSince(
        dateAndTime.parse('09-2019', 'MM-YYYY'),
        dateAndTime.parse('08-2021', 'MM-YYYY')
      ),
    },
    {
      title: `${i18next.t('experience.experiences.triad.title')}`,
      company: 'Triad Systems',
      description: `${i18next.t('experience.experiences.triad.description')}`,
      companyLink: 'http://triadsystems.com.br/',
      companyShortDetail: `${i18next.t('general.locations.saopaulo')}`,
      date: time.getTimeSince(
        dateAndTime.parse('06-2019', 'MM-YYYY'),
        dateAndTime.parse('09-2019', 'MM-YYYY')
      ),
    },
    {
      title: `${i18next.t('experience.experiences.dell.title')}`,
      company: 'Dell EMC',
      description: `${i18next.t('experience.experiences.dell.description')}`,
      companyLink: 'https://www.dellemc.com/',
      companyShortDetail: `${i18next.t('general.locations.saopaulo')}`,
      date: time.getTimeSince(
        dateAndTime.parse('07-2018', 'MM-YYYY'),
        dateAndTime.parse('06-2019', 'MM-YYYY')
      ),
    },
    {
      title: `${i18next.t('experience.experiences.clouddog.title')}`,
      company: 'CloudDog',
      description: `${i18next.t(
        'experience.experiences.clouddog.description'
      )}`,
      companyLink: 'https://www.clouddog.com.br/',
      companyShortDetail: `${i18next.t('general.locations.mogi')}`,
      date: time.getTimeSince(
        dateAndTime.parse('02-2018', 'MM-YYYY'),
        dateAndTime.parse('06-2018', 'MM-YYYY')
      ),
    },
    {
      title: `${i18next.t('experience.experiences.umc.title')}`,
      company: 'Universidade de Mogi das Cruzes',
      description: `${i18next.t('experience.experiences.umc.description')}`,
      companyLink: 'http://www.umc.br/',
      companyShortDetail: `${i18next.t('general.locations.mogi')}`,
      date: time.getTimeSince(
        dateAndTime.parse('07-2015', 'MM-YYYY'),
        dateAndTime.parse('08-2017', 'MM-YYYY')
      ),
    },
    {
      title: `${i18next.t('experience.experiences.umcInternship.title')}`,
      company: 'Universidade de Mogi das Cruzes',
      description: `${i18next.t(
        'experience.experiences.umcInternship.description'
      )}`,
      companyLink: 'http://www.umc.br/',
      companyShortDetail: `${i18next.t('general.locations.mogi')}`,
      date: time.getTimeSince(
        dateAndTime.parse('08-2014', 'MM-YYYY'),
        dateAndTime.parse('07-2015', 'MM-YYYY')
      ),
    },
  ],
};

export default experiences;
