import i18next from '../i18nextConf'
import time from '../timeFunctions'

export const careerProfile = {
  sectionTitle: i18next.t('careerProfile.title'),
  icon: null,
  description: `<p>${i18next.t('careerProfile.description').replace('my_age_func', time.getAge())}</p>`
};

export default careerProfile;
