import i18next from '../i18nextConf'

const profile = {
  name: 'Thales Alves Pereira',
  title: `${i18next.t('profile.professionalTitle')}`,
  mail: 'thales@thalesalv.es',
  wordpress: null,
  phoneNumber: null,
  website: null,
  linkedin: 'LinkedIn',
  github: 'GitHub',
  gitlab: 'GitLab',
  facebook: null,
  instagram: 'Instagram',
  imagePath: 'profile.jpg',
  twitter: null,
  languages: {
    sectionTitle: `${i18next.t('profile.languages.title')}`,
    list: [
      {
        name: `${i18next.t('profile.languages.pt.name')}`,
        level: `${i18next.t('profile.languages.pt.level')}`,
      },
      {
        name: `${i18next.t('profile.languages.en.name')}`,
        level: `${i18next.t('profile.languages.en.level')}`,
      },
      {
        name: `${i18next.t('profile.languages.fr.name')}`,
        level: `${i18next.t('profile.languages.fr.level')}`,
      },
    ],
  },
  interests: {
    sectionTitle: `${i18next.t('profile.interests.title')}`,
    list: [
      `${i18next.t('profile.interests.list.photography')}`,
      `${i18next.t('profile.interests.list.linguistics')}`,
      `${i18next.t('profile.interests.list.programming')}`,
      `${i18next.t('profile.interests.list.coffee')}`,
      'RPGs',
      'Elder Scrolls',
      'Modding',
      `${i18next.t('profile.interests.list.ai')}`,
    ],
  },
};

export default profile;
