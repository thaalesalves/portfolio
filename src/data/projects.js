import local from '../localization'

export const projects = {
  sectionTitle: local.translate('projects.title'),
  icon: null,
  description: null,
  list: [
    {
      projectName: local.translate('projects.moirai.name'),
      myTitle: local.translate('projects.moirai.myTitle'),
      company: local.translate('projects.moirai.company'),
      description: local.translate('projects.moirai.description'),
      projectLink: 'https://github.com/moirairpg',
      companyLink: 'https://thalesalv.es',
      date: '02.2023.'
    },
    {
      projectName: local.translate('projects.ipc.name'),
      myTitle: local.translate('projects.ipc.myTitle'),
      company: 'PrimeIT',
      description: local.translate('projects.ipc.description'),
      projectLink: 'https://www.ipc.be',
      companyLink: 'https://primeit.pt',
      date: '08.2023.'
    },
    {
      projectName: local.translate('projects.vizrt.name'),
      myTitle: local.translate('projects.vizrt.myTitle'),
      company: 'PrimeIT',
      description: local.translate('projects.vizrt.description'),
      projectLink: 'https://www.arisglobal.com',
      companyLink: 'https://primeit.pt',
      date: '06.2022 - 06.2023.'
    },
    {
      projectName: local.translate('projects.arisglobal.name'),
      myTitle: local.translate('projects.arisglobal.myTitle'),
      company: 'PrimeIT',
      description: local.translate('projects.arisglobal.description'),
      projectLink: 'https://www.arisglobal.com',
      companyLink: 'https://primeit.pt',
      date: '09.2021 - 05.2022.'
    },
    {
      projectName: local.translate('projects.itau.name'),
      myTitle: local.translate('projects.itau.myTitle'),
      company: 'IBM',
      description: local.translate('projects.itau.description'),
      projectLink: 'https://itau.com.br',
      companyLink: 'https://ibm.com',
      date: '09.2019 - 08.2021.'
    },
    {
      projectName: local.translate('projects.openshift-mentoring.name'),
      myTitle: local.translate('projects.openshift-mentoring.myTitle'),
      company: local.translate('projects.openshift-mentoring.company'),
      description: local.translate('projects.openshift-mentoring.description'),
      projectLink: 'https://github.com/mentoria-openshift',
      companyLink: 'https://github.com/mentoria-openshift',
      date: '09.2020 - 02.2021.'
    },
    {
      projectName: 'Grand Prognosticator',
      myTitle: local.translate('projects.grand-prognosticator.myTitle'),
      company: local.translate('projects.grand-prognosticator.company'),
      description: local.translate('projects.grand-prognosticator.description'),
      projectLink: 'https://github.com/thaalesalves/grand-prognosticator',
      companyLink: 'https://thalesalv.es'
    },
    {
      projectName: 'SiGLa',
      myTitle: local.translate('projects.sigla.myTitle'),
      company: local.translate('projects.sigla.company'),
      description: local.translate('projects.sigla.description'),
      projectLink: 'https://github.com/thaalesalves/SiGLa',
      companyLink: 'https://thalesalv.es'
    },
    {
      projectName: 'Chatbot para UESPWiki',
      myTitle: local.translate('projects.uespwiki.myTitle'),
      company: local.translate('projects.uespwiki.company'),
      description: local.translate('projects.uespwiki.description'),
      projectLink: 'https://gitlab.com/thaalesalves/DiscordBot',
      companyLink: 'https://thalesalv.es'
    }
  ]
};

export default projects;
