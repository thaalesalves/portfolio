import i18next from '../i18nextConf'

export const courses = {
  sectionTitle: i18next.t('courses.title'),
  description: null,
  icon: null,
  list: [
    {
      title: 'Red Hat OpenShift Development II: Microservices with Red Hat OpenShift',
      company: 'Red Hat',
      date: '08.2020',
      certificateLink: 'https://rol.redhat.com/rol/api/certificates/attendance/uuid/bc552a05-3c64-4afa-a463-c310bbb80963',
      certificateText: `${i18next.t('courses.certificateText')}`,
      courseLink: 'https://www.redhat.com/pt-br/services/training/do292-red-hat-openshift-development-ii'
    },
    {
      title: 'Red Hat OpenShift Development I: Containerizing Applications',
      company: 'Red Hat',
      date: '07.2020',
      certificateLink: 'https://rol.redhat.com/rol/api/certificates/attendance/uuid/df57424b-be17-4712-9acf-ec6d9c857c73',
      certificateText: `${i18next.t('courses.certificateText')}`,
      courseLink: 'https://www.redhat.com/pt-br/services/training/do288-red-hat-openshift-development-i-containerizing-applications'
    },
    {
      title: 'Introduction to Containers, Kubernetes, and Red Hat OpenShift',
      company: 'Red Hat',
      date: '06.2020',
      certificateLink: 'https://rol.redhat.com/rol/api/certificates/attendance/uuid/132e01ab-957d-4568-9a86-777ebdca94a0',
      certificateText: `${i18next.t('courses.certificateText')}`,
      courseLink: 'https://www.redhat.com/pt-br/services/training/do180-introduction-containers-kubernetes-red-hat-openshift'
    },
    {
      title: 'IBM Agile Explorer',
      company: 'IBM',
      date: '12.2019',
      certificateLink: 'https://www.youracclaim.com/badges/5b86e503-252a-4376-a829-70a680eb14f5/public_url',
      certificateText: 'Badge',
      courseLink: 'https://www.youracclaim.com/org/ibm/badge/ibm-agile-explorer'
    },
    {
      title: 'Enterprise Design Thinking Practitioner',
      company: 'IBM',
      date: '09.2019',
      certificateLink: 'https://www.youracclaim.com/badges/71b9d317-9fea-4778-86da-5420dacd6efe/public_url',
      certificateText: 'Badge',
      courseLink: 'https://www.ibm.com/design/thinking/page/courses/Practitioner'
    },
    {
      title: 'IBM Microservices - Fundamentals',
      company: 'IBM',
      date: '09.2019',
      certificateLink: 'https://www.youracclaim.com/badges/303e2671-d8e9-4729-ae21-8c7798baad7f/public_url',
      certificateText: 'Badge',
      courseLink: 'https://www.coursera.org/learn/deploy-micro-kube-ibm-cloud'
    }
  ]
};

export default courses;
