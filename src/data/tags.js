import i18next from '../i18nextConf'

export const tags = {
  sectionTitle: i18next.t('skills.title'),
  icon: null,
  list: [
    'Java',
    'Spring Boot',
    'Linux',
    'Docker',
    'AWS',
    'API',
    'OpenShift',
    'JavaScript',
    'TypeScript',
    'Microservices',
    'Artificial Intelligence',
    'Prompt engineering',
  ]
};

export default tags;
