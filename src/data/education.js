import i18next from '../i18nextConf'

export const education = {
  sectionTitle: `${i18next.t('education.title')}`,
  description: null,
  icon: null,
  list: [
    {
      course: `${i18next.t('education.bsc.course')}`,
      institution: `${i18next.t('education.bsc.institution')}`,
      institutionLink: 'http://www.umc.br',
      date: '2014 - 2018',
      description: `${i18next.t('education.bsc.description')}`
    }
  ]
};

export default education;