#!/bin/bash

portfolio_root=$(pwd)

dir_timestamp=$(date +%d-%m-%Y_%H-%m-%S)
www_dir="/var/www"
build_dir="$HOME/projects/portfolio/build"
portfolio="${www_dir}/thalesalv.es"
portfolio_bk="${www_dir}/thalesalv.es_$dir_timestamp"

echo "Atualizando dependências..."
rm -rf node_modules
npm install

echo "Buildando aplicação..."
rm -rf build
node scripts/build.js

# echo "Servindo aplicação..."
# sudo mv "${www_dir}/thalesalv.es" "${www_dir}/thalesalv.es_$dir_timestamp"
# sudo mv $build_dir "$www_dir/thalesalv.es"

# cd $portfolio_root
echo "Pronto."
exit 0
